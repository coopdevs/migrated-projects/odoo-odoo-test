[![PyPI version](https://badge.fury.io/py/odoo-odoo-test.svg)](https://badge.fury.io/py/odoo-odoo-test)

## Odoo customizations for odoo-test
Inventory files are located at: https://gitlab.com/coopdevs/odoo-test-provisioning
